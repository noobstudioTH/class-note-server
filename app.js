var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var cookieParser = require('cookie-parser')
var firebaseMiddleware = require('express-firebase-middleware');
var cors = require('cors');
var app = express();
  
const middleware = [
    bodyParser.json(), // for parsing application/json
    bodyParser.urlencoded({ extended: true }), // for parsing application/x-www-form-urlencoded
    cookieParser(), // For parsing cookies
    cors({origin: ["http://localhost:4200", "http://localhost:3000"], credentials: true, allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'], optionsSuccessStatus: 200}),
    // express.static(path.join(__dirname, 'public')),
];

app.use(middleware);

app.use('/default', express.static('uploads/default'))
app.use('/images', express.static('uploads/images'))
app.use('/audio', express.static('uploads/audio'))
// app.use(firebaseMiddleware.auth).unless({path: ['/login', 'register']});

app.use('/api', require('./routes/auth'));

app.use('/api', require('./routes'));

module.exports = app;