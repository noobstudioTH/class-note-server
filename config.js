const path = require('path');
require('dotenv').config()

module.exports = {
    path: {
        image: path.resolve('uploads/images'),
        audio: path.resolve('uploads/audio'),
    },
    resource_url: {
        image: process.env.BASE_URL+'/images/',
        audio: process.env.BASE_URL+'/audio/'
    }
}