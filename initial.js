var database = require('./utils/FirebaseConnector').database;

const subjects  = [{text: 'เคมี'}, {text: 'คณิตศาสตร์'}, {text: 'ชีวศึกษา'}, {text: 'ภาษาอังกฤษ'}, {text: 'ศิลปะ'}, {text: 'ภาษาไทย'}, {text: 'ฟิสิกส์'}]
const grades  = [{text: 'ม.4'}, {text: 'ม.5'}, {text: 'ม.6'}, {text: 'GAT/PAT'}, {text: 'ONET'}]
const levels  = [{text: 'ยาก'}, {text: 'ปานกลาง'}, {text: 'ง่าย'}]
const noteSort  = [{ value: "rating", text: "Rating"}, { value: "timestamp", text: "Created time"}, { value: "Title/Caption", text: "caption"}]
var master = database.child('masters');

master.child('subjects').set(subjects).then(() => {
    return master.child('grades').set(grades)
}).then(() => {
    return master.child('levels').set(levels)
}).then(() => {
    return master.child('note_sortable').set(noteSort);
}).then(() => {
    console.log('Has been seed to database');
    process.exit();
})