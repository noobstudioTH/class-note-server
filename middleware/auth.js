module.exports = function(req, res, next) {
    if (req.headers.authorization) {
        req.uid = req.headers.authorization
        next();
    } else if (req.cookies.uid) {
        req.uid = req.cookies.uid
        next();
    } else {
        res.status(405).json({ message: 'Please login before' });
    }
}