let database = require('../utils/FirebaseConnector').database;

module.exports = async (seriesRef) => {
    let response = await seriesRef.child('notes').once('value');
    let notes = response.exists() ? response.val() : [];
    let subjects = {};
    
    for (let note of notes) {
        let response = await database.child('notes/' + note + '/subject').once("value");
        subjects[response.val()] = true;
    }

    await seriesRef.update({subjects});

    return true;
}