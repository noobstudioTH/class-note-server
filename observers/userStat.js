let _ = require('lodash');
let database = require('../utils/FirebaseConnector').database;

const updateNoteCount = async (userId, noteRef) => {
    let snapshot = await noteRef.orderByChild("owner").equalTo(userId).once("value");
    await database.child('users/' + userId + '/stat').update({note_count: snapshot.numChildren()});
    return true;
}

const updatSeriesCount = async (userId, seriesRef) => {
    let snapshot = await seriesRef.orderByChild("owner").equalTo(userId).once("value");
    await database.child('users/' + userId + '/stat').update({series_count: snapshot.numChildren()});
    return true;
}

const updateRatingCount = async (userId, noteRef) => {
    const snapshot = await noteRef.orderByChild("owner").equalTo(userId).once("value");
    const notes = snapshot.val();
    let rating = 0;
    let count = 0;
    for (let key in notes) {
        if (notes[key].rating != undefined) {
            const tempRating = _.reduce(notes[key].rating, (initial, value) => { return initial+value }, 0) / Object.keys(notes[key].rating).length
            rating += tempRating;
            count++;
        }
    }
    try {
        await database.child('users/' + userId + '/stat').update({rating_avg: rating/count});
    } catch(exception){
        console.log(exception);
    }
    return true;
}

module.exports = {
    updateNoteCount,
    updatSeriesCount,
    updateRatingCount
}