const express = require('express');
const authUser = require('./../../middleware/auth');
const AdminNoteApprovementService = require('./../../services/admin/admin-dashboard/note-approvement');
const AdminNoteService = require('./../../services/admin/admin-note');
const AdminNoteRatingService = require('./../../services/admin/admin-dashboard/note-rating');
const AdminNoteReportService = require('./../../services/admin/admin-dashboard/note-report');
let router = express.Router();

router.post('/admin/dashboard/note-status-all', authUser, async (req, res) => {
    let adminNoteService = new AdminNoteService();
    let response = await adminNoteService.getList(req.body, 'notesMapping');
    res.json(response);
});

router.post('/admin/dashboard/note-status-summary', authUser, async (req, res) => {
    let adminNoteApprovementService = new AdminNoteApprovementService();
    let response = await adminNoteApprovementService.getNotesByStatusSummary(req.body);
    res.json(response);
});

router.post('/admin/dashboard/note-rating-summary', authUser, async (req, res) => {
    let adminNoteRatingService = new AdminNoteRatingService();
    let response = await adminNoteRatingService.getNotesByRatingSummary(req.body);
    res.json(response);
});

router.post('/admin/dashboard/note-report-summary', authUser, async (req, res) => {
    let adminNoteReportService = new AdminNoteReportService();
    let response = await adminNoteReportService.getNotesByReportSummary(req.body);
    res.json(response);
});

module.exports = router;