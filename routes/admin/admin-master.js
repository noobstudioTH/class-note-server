const express = require('express');
const AdminMasterService = require('../../services/admin/admin-master');
const authUser = require('./../../middleware/auth');

let router = express.Router();

const master = {
    subject: {
        key: 'subjects',
        orderbyKey: 'subject'
    },
    grade: {
        key: 'grades',
        orderbyKey: 'grade'
    },
    level: {
        key: 'levels',
        orderbyKey: 'level'
    }
};

router.post('/admin/master/data/:key', authUser, async (req, res) => {
    let body = req.body;
    let adminMasterService = new AdminMasterService();
    let response = await adminMasterService.get(body, master[req.params.key].key, master[req.params.key].orderbyKey);
    res.json(response);
});

router.post('/admin/master/:key', authUser, async (req, res) => {
    let body = req.body;
    let adminMasterService = new AdminMasterService();
    if (await adminMasterService.checkDuplicated(body, master[req.params.key].key)) {
        return res.status(422).json({
            errors: 'Duplicated Code'
        })
    }
    let response = await adminMasterService.create(body, master[req.params.key].key);
    res.json({
        message: 'Data has been created.'
    })
});

router.put('/admin/master/:key', authUser, async (req, res) => {
    let body = req.body;
    let adminMasterService = new AdminMasterService();
    let response = await adminMasterService.update(body, master[req.params.key].key);
    res.json({
        message: 'Data has been updated.'
    })
});

router.post('/admin/master/delete/:key', authUser, async (req, res) => {
    let body = req.body;
    let adminMasterService = new AdminMasterService();
    let response = await adminMasterService.delete(body, master[req.params.key].key);
    res.json({
        message: 'Data has been deleted.'
    })
});

module.exports = router;