const express = require('express');
const AdminNoteService = require('./../../services/admin/admin-note');
const authUser = require('./../../middleware/auth');

let router = express.Router();

router.get('/admin/notes', authUser, async (req, res) => {
    let adminNoteService = new AdminNoteService();
    let response = await adminNoteService.getAll(req.body, 'notesMapping');
    res.json(response);
});

router.post('/admin/notes', authUser, async (req, res) => {
    let adminNoteService = new AdminNoteService();
    let response = await adminNoteService.getList(req.body, 'notesMapping');
    res.json(response);
});

router.post('/admin/notes/report', authUser, async (req, res) => {
    let adminNoteService = new AdminNoteService();
    let response = await adminNoteService.getList(req.body, 'reportMapping');
    res.json(response);
});

router.get('/admin/notes/approve/:id', authUser, async (req, res) => {
    let adminNoteService = new AdminNoteService();
    let response = await adminNoteService.updateStatus(req.params.id, 'approve');
    res.json(response);
});

router.get('/admin/notes/reject/:id', authUser, async (req, res) => {
    let adminNoteService = new AdminNoteService();
    let response = await adminNoteService.updateStatus(req.params.id, 'reject');
    res.json(response);
});

router.get('/admin/notes/ban/:id', authUser, async (req, res) => {
    let adminNoteService = new AdminNoteService();
    let response = await adminNoteService.updateStatus(req.params.id, 'ban');
    res.json(response);
});

module.exports = router;