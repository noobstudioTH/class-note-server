const express = require('express');
const authUser = require('./../../middleware/auth');
const AdminReportService = require('./../../services/admin/admin-report');
const AdminRankingService = require('./../../services/admin/admin-ranking');

let router = express.Router();

router.post('/admin/notes/overview', authUser, async (req, res) => {
    let adminReportService = new AdminReportService();
    let response = await adminReportService.getNoteOverview(req.body);
    res.json(response);
});

router.post('/admin/ranking/notes', authUser, async (req, res) => {
    let adminRankingService = new AdminRankingService();
    let response = await adminRankingService.getNotes(req.body);
    res.json(response);
});

router.post('/admin/ranking/search', authUser, async (req, res) => {
    let adminRankingService = new AdminRankingService();
    let response = await adminRankingService.getSearchs(req.body);
    res.json(response);
});

module.exports = router;