const express = require('express');
const AdminSeriesService = require('./../../services/admin/admin-series');
const {
    checkSchema,
    validationResult
} = require('express-validator/check');
const authUser = require('./../../middleware/auth');

let router = express.Router();
let adminSeriesService = new AdminSeriesService();

router.post('/admin/series', authUser, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({
        errors: errors.mapped()
    });
    let response = await adminSeriesService.getList(req.body);
    res.json(response);
});

router.get('/admin/series-master', authUser, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({
        errors: errors.mapped()
    });
    let response = await adminSeriesService.getSeriesAsMaster(req.body);
    res.json(response);
});

module.exports = router;