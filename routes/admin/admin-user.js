const express = require('express');
const authUser = require('./../../middleware/auth');
const AdminUserService = require('./../../services/admin/admin-user');

let router = express.Router();
let adminUserService = new AdminUserService();

router.post('/admin/users', authUser, async (req, res) => {
    let response = await adminUserService.getList(req.body);
    res.json(response);
});

module.exports = router;