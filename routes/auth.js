var express = require("express");
var router = express.Router();
const authUser = require("../middleware/auth");

const AuthService = require("../services/auth");
const UserService = require("../services/user");
const NoteService = require("../services/note");
const SeriesService = require("../services/series");
var authService = new AuthService();
var userService = new UserService();
var noteService = new NoteService();
var seriesService = new SeriesService();

router.get("/auth/token/user", (req, res) => {
  authService.getUserToken().then(response => {
    res.json({ token: response });
  });
});

router.get("/auth/token/admin", (req, res) => {
  authService.getAdminToken().then(response => {
    res.json({ token: response });
  });
});

router.get("/auth", authUser, (req, res) => {
  userService.find(req.uid).then(response => {
    response.id = req.uid;
    res.json(response);
  });
});

router.get("/auth/notes", authUser, async (req, res) => {
  let response = await noteService.all(req.query, req.uid, true);
  res.json(response);
});

router.get("/auth/series", authUser, async (req, res) => {
  let response = await seriesService.all(req.query, req.uid, true);
  res.json(response);
});

router.post("/login", async (req, res) => {
  try {
    let response = await userService.find(req.body.uid);
    res.cookie("uid", req.body.uid, { maxAge: +process.env.COOKIES_MAX_AGE });
    response.id = req.body.uid;
    res.json({ data: response, message: "Logged in" });
  } catch (e) {
    console.log(e);
    res.clearCookie("uid");
    res.status(401).json({ message: e });
  }
});

router.post("/logout", (req, res) => {
  res.clearCookie("uid");
  res.json({ message: "Logged out" });
});

module.exports = router;
