var express = require('express')
var router = express.Router();
var fs = require('fs');

// Read each file in the routes directory
fs.readdirSync(__dirname).forEach(function (route) {
    if (route === 'index.js' || route === 'auth.js' || route === 'admin') return;
    console.log('Loading route ' + route);
    router.use(require('./' + route));
});
fs.readdirSync(__dirname + '/admin').forEach(function (route) {
    console.log('Loading route ' + route);
    router.use(require('./admin/' + route));
});

module.exports = router;