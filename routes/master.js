var express = require( 'express' )
var router = express.Router();

const MasterService = require('../services/master');
var masterService = new MasterService;

router.get('/grades', (req, res) => {
    masterService.grades().then(response => {
        res.json(response);
    });
})

router.get('/levels', (req, res) => {
    masterService.levels().then(response => {
        res.json(response);
    });
})

router.get('/subjects', (req, res) => {
    masterService.subjects().then(response => {
        res.json(response);
    });
})

router.get('/privacy', (req, res) => {
    masterService.privacy().then(response => {
        res.json(response);
    });
})

router.get('/status', (req, res) => {
    masterService.status().then(response => {
        res.json(response);
    });
})

module.exports = router;