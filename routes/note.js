const express = require("express");
const multer = require("multer");
const path = require("../config").path;
const { check, param, validationResult } = require("express-validator/check");
const {
  isHasSubject,
  isHasGrade,
  isHasLevel,
  isHasPrivacy
} = require("../validator/checkMaster");
const { isHasSeries } = require("../validator/checkSeries");
const {
  isHasNote,
  isHasMedia,
  isHasReview
} = require("../validator/checkNote");
const NoteService = require("../services/note");
const ReviewService = require("../services/review");
const MasterService = require("../services/master");
const NoteSchema = require("../schema-validation/note");
const authUser = require("../middleware/auth");

const storageImage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, path.image);
  },
  filename: function(req, file, cb) {
    cb(
      null,
      "note-" + req.params.id + Date.now() + "." + file.mimetype.split("/")[1]
    );
  }
});
const storageAudio = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, path.audio);
  },
  filename: function(req, file, cb) {
    let fileNameArray = file.originalname.split(".");
    cb(
      null,
      "note-" +
        req.params.id +
        Date.now() +
        "." +
        fileNameArray[fileNameArray.length - 1]
    );
  }
});
const uploadImage = multer({ storage: storageImage });
const uploadAudio = multer({ storage: storageAudio });

let router = express.Router();
let masterService = new MasterService();
let noteService = new NoteService();
let reviewService = new ReviewService();

// List
router.get("/notes", authUser, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ errors: errors.mapped() });

  let response = await noteService.all(req.query, req.uid);
  res.json(response);
});

router.get("/notes/sorts", async (req, res) => {
  let response = await masterService.noteSortable();
  res.json(response);
});

// Get
router.get("/notes/:id", authUser, (req, res) => {
  noteService.find(req.params.id, req.uid).then(response => {
    res.json(response);
  });
});

// Create
router.post(
  "/notes",
  [
    check("caption")
      .isLength({ min: 1 })
      .withMessage("Caption shouldn't be empty"),
    check("subject")
      .custom(async value => {
        return await isHasSubject(value);
      })
      .withMessage("Invalid subject"),
    check("grade")
      .custom(async value => {
        return await isHasGrade(value);
      })
      .withMessage("Invalid grade"),
    check("level")
      .custom(async value => {
        return await isHasLevel(value);
      })
      .withMessage("Invalid level"),
    check("privacy")
      .custom(async value => {
        return await isHasPrivacy(value);
      })
      .withMessage("Invalid privacy"),
    // check("series_id")
    //   .custom(async value => {
    //     if (value == undefined) return true;
    //     return await isHasSeries(value);
    //   })
    //   .withMessage("Invalid series id")
  ],
  authUser,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    let response = await noteService.create(req.body, req.uid);
    res.json({
      data: { key: response },
      message: "Note has been created."
    });
  }
);

// Update
router.put("/notes/:id", (req, res) => {
  noteService.update(req.params.id, req.body).then(response => {
    res.json({
      data: response,
      message: "Note has been updated."
    });
  });
});

// Remove
router.delete("/notes/:id", authUser, async (req, res) => {
  await noteService.remove(req.params.id, req.uid);

  return res.json({
    message: "Note has been removed."
  });
});

// Favorite
router.post(
  "/notes/:id/favorite",
  [
    param("id")
      .custom(async value => {
        return await isHasNote(value);
      })
      .withMessage("Not found note.")
  ],
  authUser,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    let response = await noteService.favorite(req.params.id, req.uid);
    res.json({
      is_favorited: response,
      message: "Has been toggle favorite."
    });
  }
);

// Rating
router.post(
  "/notes/:id/rating",
  [
    param("id")
      .custom(async value => {
        return await isHasNote(value);
      })
      .withMessage("Not found note.")
  ],
  authUser,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    let response = await noteService.rating(
      req.params.id,
      req.uid,
      req.body.rating
    );
    res.json({
      message: "Has been add rating note."
    });
  }
);

// Upload image (Create media)
router.post(
  "/notes/:id/media",
  [
    param("id")
      .custom(async value => {
        return await isHasNote(value);
      })
      .withMessage("Not found note.")
  ],
  uploadImage.single("image"),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    let response = await noteService.uploadImage(
      req.params.id,
      req.file.filename
    );
    res.json({
      data: { media_id: response },
      message: "Image note has been upload."
    });
  }
);

// Remove media
router.delete(
  "/notes/:id/media/:mediaId",
  [
    param("id")
      .custom(async value => {
        return await isHasNote(value);
      })
      .withMessage("Not found note."),
    param("mediaId")
      .custom(async (value, { req }) => {
        return await isHasMedia(req.params.id, value);
      })
      .withMessage("Not found media.")
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    await noteService.removeMedia(req.params.id, req.params.mediaId);
    res.json({
      message: "Media note has been removed."
    });
  }
);

// Upload audio
router.post(
  "/notes/:id/media/:mediaId/audio",
  [
    param("id")
      .custom(async value => {
        return await isHasNote(value);
      })
      .withMessage("Not found note."),
    param("mediaId")
      .custom(async (value, { req }) => {
        return await isHasMedia(req.params.id, value);
      })
      .withMessage("Not found media.")
  ],
  uploadAudio.single("audio"),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    let response = await noteService.uploadAudio(
      req.params.id,
      req.params.mediaId,
      req.file.filename
    );
    res.json({
      message: "Audio has been upload."
    });
  }
);

// Remove audio
router.delete(
  "/notes/:id/media/:mediaId/audio",
  [
    param("id")
      .custom(async value => {
        return await isHasNote(value);
      })
      .withMessage("Not found note."),
    param("mediaId")
      .custom(async (value, { req }) => {
        return await isHasMedia(req.params.id, value);
      })
      .withMessage("Not found media.")
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    await noteService.removeAudio(req.params.id, req.params.mediaId);
    res.json({
      message: "Audio has been removed."
    });
  }
);

// Get list reviews
router.get(
  "/notes/:id/reviews",
  [
    param("id")
      .custom(async value => {
        return await isHasNote(value);
      })
      .withMessage("Not found note.")
  ],
  authUser,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    let response = await reviewService.all(req.params.id, req.query, req.uid);
    res.json(response);
  }
);

// Post reviews
router.post(
  "/notes/:id/reviews",
  [
    param("id")
      .custom(async value => {
        return await isHasNote(value);
      })
      .withMessage("Not found note.")
  ],
  authUser,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    let response = await reviewService.post(req.params.id, req.body, req.uid);
    res.json({ message: "User has been post review to note" });
  }
);

// Toggle like reviews
router.post(
  "/notes/:id/reviews/:reviewId/like",
  [
    param("id")
      .custom(async value => {
        return await isHasNote(value);
      })
      .withMessage("Not found note."),
    param("reviewId")
      .custom(async (value, { req }) => {
        return await isHasReview(req.params.id, value);
      })
      .withMessage("Not found review.")
  ],
  authUser,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    let response = await reviewService.like(
      req.params.id,
      req.params.reviewId,
      req.uid
    );
    res.json({ is_liked: response, message: "Has been toggle like." });
  }
);

router.post("/notes/:id/report", authUser, async (req, res) => {
  await noteService.report(req.params.id, req.body.message);
  res.json(`Note ${req.params.id} has been reported`);
});

module.exports = router;
