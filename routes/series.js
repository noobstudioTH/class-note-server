var express = require("express");
var router = express.Router();
var authUser = require("../middleware/auth");
const { isHasNote } = require("../validator/checkNote");
const { isHasSeries } = require("../validator/checkSeries");
const { check, param, validationResult } = require("express-validator/check");

const SeriesService = require("../services/series");
var seriesService = new SeriesService();

router.get("/series", authUser, async (req, res) => {
  let response = await seriesService.all(req.query, req.uid);
  res.json(response);
});

router.get("/series/:id", authUser, (req, res) => {
  seriesService.find(req.params.id, req.uid).then(response => {
    res.json(response);
  });
});

router.post(
  "/series",
  [
    check("name")
      .isLength({ min: 1 })
      .withMessage("Name shouldn't be empty")
  ],
  authUser,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    let response = await seriesService.create(req.body, req.uid);
    res.json({
      data: { key: response },
      message: "Series has been created."
    });
  }
);

router.put("/series", async (req, res) => {
  let response = await seriesService.updateSeries(req.body);
  res.json({
    message: "Series has been updated."
  });
});

router.put("/series/:id", (req, res) => {
  seriesService.update(req.params.id, req.body).then(response => {
    res.json({
      message: "Series has been updated."
    });
  });
});

router.delete("/series/:id", authUser, (req, res) => {
  seriesService.remove(req.params.id).then(async response => {
    await seriesService.removeSeries(req.uid);
    res.json({
      message: "Series has been removed."
    });
  });
});

router.post(
  "/series/:id/notes",
  [
    param("id")
      .custom(async value => {
        if (value == undefined) return true;
        return await isHasSeries(value);
      })
      .withMessage("Invalid series id")
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    for (let noteId of req.body) {
      if ((await isHasNote(noteId)) == false)
        return res.status(422).json({ errors: { msg: "Note found note" } });
    }

    await seriesService.addNotes(req.params.id, req.body);

    res.json({
      message: "Has been added notes to series id " + req.params.id
    });
  }
);

router.put(
  "/series/:id/notes",
  [
    param("id")
      .custom(async value => {
        if (value == undefined) return true;
        return await isHasSeries(value);
      })
      .withMessage("Invalid series id")
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    await seriesService.removeNotes(req.params.id, req.body);

    res.json({
      message: "Has been removed notes from series id " + req.params.id
    });
  }
);

// Favorite
router.post(
  "/series/:id/favorite",
  [
    param("id")
      .custom(async value => {
        if (value == undefined) return true;
        return await isHasSeries(value);
      })
      .withMessage("Invalid series id")
  ],
  authUser,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    let response = await seriesService.favorite(req.params.id, req.uid);
    res.json({
      is_favorited: response,
      message: "Has been toggle favorite."
    });
  }
);

// Rating
router.post(
  "/series/:id/rating",
  [
    param("id")
      .custom(async value => {
        if (value == undefined) return true;
        return await isHasSeries(value);
      })
      .withMessage("Invalid series id")
  ],
  authUser,
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.mapped() });

    seriesService
      .rating(req.params.id, req.uid, req.body.rating)
      .then(response => {
        res.json({
          message: "Has been add rating series."
        });
      });
  }
);

module.exports = router;
