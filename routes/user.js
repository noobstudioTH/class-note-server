var express = require('express')
var router = express.Router();
const authUser = require('../middleware/auth');

const UserService = require('../services/user');
var userService = new UserService;



router.get('/users', (req, res) => {
    userService.all(req.params).then(response => {
        res.json(response);
    });
})

router.get('/users/:id', authUser, (req, res) => {
    userService.find(req.params.id, req.uid).then(response => {
        res.json(response);
    });
})

router.post('/users', (req, res) => {
    userService.create(req.body).then(response => {
        res.json({
            data: {
                key: response
            },
            message: 'users has been created.'
        });
    });
})

router.put('/users/:id', (req, res) => {
    userService.update(req.params.id, req.body).then(response => {
        res.json({
            message: 'users has been updated.'
        });
    })
})

router.delete('/users/:id', (req, res) => {
    userService.remove(req.params.id).then(response => {
        res.json({
            message: 'users has been removed.'
        });
    });
})

router.post('/users/:id/follow', authUser, async (req, res) => {
    let response = await userService.follow(req.params.id, req.uid);
    res.json({
        is_followed: response,
        message: 'user has been followed.'
    });
})

module.exports = router;