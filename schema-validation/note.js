module.exports = {
    create: {
        caption: {
            isLength: {
                errorMessage: 'Caption shouldn\'t be empty',
                options: { min: 1 }
            }
        },
        password: {
            isLength: {
                errorMessage: 'Password should be at least 7 chars long',
                // Multiple options would be expressed as an array
                options: { min: 7 }
            }
        },
    }
}