const database = require('./../../../utils/FirebaseConnector').database;
const moment = require('moment');

exports = module.exports = class AdminNoteApprovementService {

    constructor() {
        this.dataItemList = [{
            name: 'Approve',
            value: 0
        }, {
            name: 'Reject',
            value: 0
        }, {
            name: 'Pending',
            value: 0
        }, {
            name: 'Ban',
            value: 0
        }];
    }

    async getNotesByStatusSummary(options) {
        try {
            let noteRef = database.child('notes');
            let response = await noteRef.once('value');
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            let counter = helper.findNoteInTime(response, options);
            helper.mappingData(this.dataItemList, counter);
        } catch (exception) {
            console.log(exception);
        } finally {
            return this.dataItemList;
        }
    }

    
}

let helper = {
    findNoteInTime: (response, options) => {
        const today = moment();
        const startOfTime = moment().startOf(options.queryBy);
        const count = {
            approve: 0,
            reject: 0,
            pending: 0,
            ban: 0
        };
        try {
            for (let key in response) {
                let note = response[key];
                if (note.timestamp && moment(note.timestamp).isBetween(startOfTime, today)) {
                    switch (note.status) {
                        case 'approve':
                            count.approve++;
                            break;
                        case 'reject':
                            count.reject++;
                            break;
                        case 'pending':
                            count.pending++;
                            break;
                        case 'ban':
                            count.ban++;
                            break;
                        default:
                            break;
                    }
                }
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return count;
        }
    },
    mappingData: (dataItemList, count) => {
        try {
            dataItemList.find(dataItem => dataItem.name === 'Approve').value = count.approve;
            dataItemList.find(dataItem => dataItem.name === 'Reject').value = count.reject;
            dataItemList.find(dataItem => dataItem.name === 'Pending').value = count.pending;
            dataItemList.find(dataItem => dataItem.name === 'Ban').value = count.ban;
        } catch (exception) {
            console.log(exception);
        }
    }
}