const database = require('./../../../utils/FirebaseConnector').database;
const moment = require('moment');

exports = module.exports = class AdminNoteRatingService {

    constructor() {
        this.dataItemList = [{
            name: 'Rating-0',
            value: 0
        }];
    }

    async getNotesByRatingSummary(options) {
        try {
            let noteRef = database.child('notes');
            let response = await noteRef.once('value');
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            let counter = helper.findNoteInTime(response, options);
            let items = helper.mappingData(counter);
            this.dataItemList = items.length > 0 ? items : this.dataItemList;
        } catch (exception) {
            console.log(exception);
        } finally {
            return this.dataItemList;
        }
    }


}

let helper = {
    findNoteInTime: (response, options) => {
        const today = moment();
        const startOfTime = moment().startOf(options.queryBy);
        const count = {};
        try {
            for (let key in response) {
                let note = response[key];
                if (note.timestamp && moment(note.timestamp).isBetween(startOfTime, today)) {
                    let rating = 0;
                    if (note.rating) {
                        rating = Object.values(note.rating)[0];
                    }
                    let name = `Rating-${rating}`;
                    if (count[name]) {
                        count[name] = ++count[name];
                    } else {
                        count[name] = 1;
                    }
                }
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return count;
        }
    },
    mappingData: (count) => {
        let dataItemList = [];
        try {
            for (let key in count) {
                dataItemList.push({
                    name: key,
                    value: count[key]
                });
            }
            dataItemList.sort((a, b) => a.name.split('-')[1] - b.name.split('-')[1]);
        } catch (exception) {
            console.log(exception);
        } finally {
            return dataItemList;
        }
    }
}