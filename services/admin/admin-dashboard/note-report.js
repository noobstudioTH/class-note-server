const database = require('./../../../utils/FirebaseConnector').database;
const moment = require('moment');

exports = module.exports = class AdminNoteReportService {

    constructor() {
        this.dataItemList = [{
            name: 'Notes',
            series: [{
                name: 'Reportes Notes',
                value: 0
            }, {
                name: "Notes",
                value: 0
            }]
        }];
    }

    async getNotesByReportSummary(options) {
        try {
            let noteRef = database.child('notes');
            let response = await noteRef.once('value');
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            let counter = helper.findNoteInTime(response, options);
            helper.mappingData(this.dataItemList, counter);
        } catch (exception) {
            console.log(exception);
        } finally {
            return this.dataItemList;
        }
    }


}

let helper = {
    findNoteInTime: (response, options) => {
        const today = moment();
        const startOfTime = moment().startOf(options.queryBy);
        const count = {
            total: 0,
            report: 0
        };
        try {
            for (let key in response) {
                let note = response[key];
                if (note.timestamp && moment(note.timestamp).isBetween(startOfTime, today)) {
                    count.total++;
                    if (note.report) {
                        count.report++;
                    }
                }
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return count;
        }
    },
    mappingData: (dataItemList, count) => {
        try {
            dataItemList[0].series[0].value = count.report;
            dataItemList[0].series[1].value = count.total;
        } catch (exception) {
            console.log(exception);
        }
    }
}