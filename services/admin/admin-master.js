const database = require('./../../utils/FirebaseConnector').database;
const _ = require('lodash');

exports = module.exports = class AdminMasterService {
    constructor() {
        this.dataItemList = {
            total: 0,
            data: []
        }
    }

    async get(options, key, orderByKey) {
        try {
            let response = await database.child('masters').child(key).once('value');
            let noteResponse = await database.child('notes').once('value');
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            if (noteResponse && noteResponse.exists()) noteResponse = noteResponse.val();
            this.dataItemList.total = response.length;
            response = response.slice(options.start, options.end);
            for (let resData of response) {
                resData.selected = 0;
                try {
                    for (let key in noteResponse) {
                        let note = noteResponse[key];
                        if (note[orderByKey] === resData.code) {
                            resData.selected++;
                        }
                    }
                } catch (exception) {}
            }
            response = helpers.sorting(options, response);
            this.dataItemList.data = response;
        } catch (exception) {
            console.log(exception);
        } finally {
            return this.dataItemList;
        }
    }

    async checkDuplicated(data, key) {
        let isDup = false;
        try {
            let response = await database.child('masters').child(key).orderByChild('code').equalTo(data.code).once('value');
            if (response.exists()) {
                isDup = true;
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return isDup;
        }
    }

    async create(data, key) {
        let master = null;
        try {
            let databaseRef = database.child('masters').child(key);
            let response = await databaseRef.once('value')
            if (response && response.exists()) {
                response = response.val();
                response.push(data);
                master = await databaseRef.set(response);
            } else {
                master = await databaseRef.set([data]);
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return master;
        }
    }

    async update(data, key) {
        let master = null;
        try {
            let databaseRef = database.child('masters').child(key);
            let response = await databaseRef.once('value')
            if (response && response.exists()) {
                response = response.val();
                let index = _.findIndex(response, {
                    code: data.code
                });
                response.splice(index, 1, data);
                master = await databaseRef.set(response);
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return master;
        }
    }

    async delete(data, key) {
        let master = null;
        try {
            let databaseRef = database.child('masters').child(key);
            let response = await databaseRef.once('value')
            if (response && response.exists()) {
                response = response.val();
                for (let item of data) {
                    let index = _.findIndex(response, {
                        code: item.code
                    });
                    response.splice(index, 1);
                }
                master = await databaseRef.set(response);
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return master;
        }
    }
}

let helpers = {
    sorting: (options, datas) => {
        try {
            if (options && options.sortBy && options.sortType) {
                datas = _.orderBy(datas, [data => _.isNumber(data[options.sortBy]) ? data[options.sortBy] : data[options.sortBy].toLowerCase()], [options.sortType]);
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return datas;
        }
    }
}