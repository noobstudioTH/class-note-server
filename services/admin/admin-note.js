const database = require('./../../utils/FirebaseConnector').database;
const moment = require('moment');
const _ = require('lodash');

exports = module.exports = class AdminNoteService {
    constructor() {
        this.dataItemList = {
            total: 0,
            data: []
        }
    }

    async getAll() {
        try {
            let noteRef = database.child('notes');
            let response = await noteRef.once('value');
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            let notes = [];
            for (let key in response) {
                let note = response[key];
                note.id = key;
                notes.push(note);
            }
            this.dataItemList.total = notes.length;
            this.dataItemList.data = notes;
        } catch (exception) {
            console.log(exception);
        } finally {
            return this.dataItemList;
        }
    }

    async getList(options, callback) {
        try {
            let noteRef = database.child('notes');
            let response = await helpers.searchByCriteria(noteRef, options);
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            let notes = helpers.transformFilter(response, options);
            notes = await helpers[callback](database, notes);
            notes = helpers.filterByOwner(options, notes);
            notes = helpers.sorting(options, notes);
            this.dataItemList.total = notes.length;
            notes = notes.slice(options.start, options.end);
            this.dataItemList.data = notes;
        } catch (exception) {
            console.log(exception);
        } finally {
            return this.dataItemList;
        }
    }

    async updateStatus(id, status) {
        let res = await database.child('notes/' + id).update({
            status: status
        });
        return true;
    }

}

let helpers = {
    searchByCriteria: async (noteRef, options) => {
        let response = null;
        try {
            if (options) {
                options.start = options.start || 0;
                options.end = options.end || 10;
                let search = options.search;
                if (search && search.privacy) {
                    response = await noteRef.orderByChild('privacy').equalTo(search.privacy).once('value');
                    delete search.privacy;
                } else if (search && search.status) {
                    response = await noteRef.orderByChild('status').equalTo(search.status).once('value');
                    delete search.status;
                } else {
                    response = await noteRef.once('value');
                }
            } else {
                response = await noteRef.once('value');
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return response;
        }
    },
    transformFilter: (response, options) => {
        let notes = [];
        try {
            for (let key in response) {
                let note = response[key];
                note.id = key;
                notes.push(note);
            }
            if (options) {
                let search = options.search;
                if (search) {
                    if (search.key) {
                        notes = _.filter(notes, note => {
                            if (note.id) {
                                return note.id.indexOf(search.key) > -1
                            } else {
                                return false;
                            }
                        });
                    }
                    if (search.caption) {
                        notes = _.filter(notes, note => {
                            if (note.caption) {
                                return note.caption.indexOf(search.caption) > -1
                            } else {
                                return false;
                            }
                        });
                    }
                    if (search.queryBy) {
                        const today = moment();
                        const startOfTime = moment().startOf(search.queryBy);
                        notes = _.filter(notes, note => {
                            if (note.timestamp && moment(note.timestamp).isBetween(startOfTime, today)) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                    }
                }
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return notes;
        }
    },
    filterByOwner: (options, notes) => {
        try {
            if (options && options.search && options.search.writerName) {
                notes = _.filter(notes, note => {
                    if (note.writername) {
                        return note.writername.indexOf(options.search.writerName) > -1
                    } else {
                        return false;
                    }
                });
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return notes;
        }
    },
    sorting: (options, notes) => {
        try {
            if (options && options.sortBy && options.sortType) {
                notes = _.orderBy(notes, [note => note[options.sortBy] ? note[options.sortBy].toLowerCase() : note[options.sortBy]], [options.sortType]);
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return notes;
        }
    },
    notesMapping: async (database, notes) => {
        try {
            let users = await database.child('users').once('value');
            users = users.val();
            for (let note of notes) {
                note.name = note.caption;
                helpers.getOwner(users, note);
                note.addedDate = moment(note.timestamp).format('L');
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return notes;
        }
    },
    reportMapping: async (database, notes) => {
        let notesTemp = [];
        try {
            let users = await database.child('users').once('value');
            let reports = await database.child('report').once('value');
            users = users.val();
            reports = reports.val();
            for (let note of notes) {
                note.name = note.caption;
                helpers.getOwner(users, note);
                note.addedDate = moment(note.timestamp).format('L');
                note.reportedNumber = 0;
                helpers.getReport(reports, note, notesTemp);
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return notesTemp;
        }
    },
    getOwner: (users, note) => {
        try {
            if (users) {
                let owner = users[note.owner];
                note.writername = owner ? owner.display_name : null;
                note.ownerDetail = owner;
            }
        } catch (exception) {
            console.log(exception);
        }
    },
    getReport: (reports, note, notesTemp) => {
        try {
            if (reports) {
                let report = reports[note.report];
                if (report && Object.keys(report).length > 0) {
                    note.report = [];
                    let index = 1;
                    for (let key in report) {
                        try {
                            let _report = report[key];
                            let dateTime = new moment(_report.timestamp);
                            _report.index = index++;
                            _report.date = dateTime.format('DD/MM/YYYY');
                            _report.time = dateTime.format('HH:mm:ss');
                            note.report.push(_report);
                        } catch (exception) {
                            console.log(exception);
                        }
                    }
                    note.reportedNumber = note.report.length;
                    notesTemp.push(note);
                }
            }
        } catch (exception) {
            console.log(exception);
        }
    }
}