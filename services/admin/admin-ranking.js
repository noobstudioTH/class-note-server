const database = require('./../../utils/FirebaseConnector').database;
const moment = require('moment');
const _ = require('lodash');

exports = module.exports = class AdminRankingService {
    constructor() {
        this.dataItemList = {
            total: 0,
            data: []
        }
    }

    async getNotes(options) {
        try {
            let noteRef = database.child('notes');
            let response = await noteRef.once('value');
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            let notes = helpers.findNoteInTime(options, response);
            notes = helpers.sorting(options, notes);
            notes = helpers.ranking(notes);
            this.dataItemList.total = notes.length;
            notes = notes.slice(options.start, options.end);
            notes = await helpers.noteDataMapping(notes);
            this.dataItemList.data = notes;
        } catch (exception) {
            console.log(exception);
        } finally {
            return this.dataItemList;
        }
    }

    async getSearchs(options) {
        try {
            let searchKeyRef = database.child('searchKey');
            let response = await searchKeyRef.once('value');
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            let searchs = helpers.findSearchInTime(options, response);
            searchs = helpers.sorting(options, searchs);
            searchs = helpers.ranking(searchs);
            this.dataItemList.total = searchs.length;
            searchs = searchs.slice(options.start, options.end);
            this.dataItemList.data = searchs;
        } catch (exception) {
            console.log(exception);
        } finally {
            return this.dataItemList;
        }
    }
}

let helpers = {
    findNoteInTime: (options, response) => {
        const notes = [];
        try {
            const today = moment();
            const startOfTime = moment().startOf(options.queryBy);
            for (let key in response) {
                let note = response[key];
                if (note.timestamp && moment(note.timestamp).isBetween(startOfTime, today)) {
                    note.id = key;
                    note.name = note.caption;
                    let rating = note.rating ? Object.values(note.rating) : null;
                    note.rating = rating && rating.length > 0 ? rating[0] : 0;
                    note.favorite = note.favorites ? note.favorites.length : 0;
                    note.view = 0;
                    note.transaction = 0;
                    notes.push(note);
                }
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return notes;
        }
    },
    findSearchInTime: (options, response) => {
        const searchs = [];
        try {
            const today = moment();
            const startOfTime = moment().startOf(options.queryBy);
            for (let key in response) {
                let search = response[key];
                if (search.timestamp && moment(search.timestamp).isBetween(startOfTime, today)) {
                    search.id = key;
                    searchs.push(search);
                }
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return searchs;
        }
    },
    sorting: (options, data) => {
        try {
            if (options && options.sortBy) {
                data = _.orderBy(data, [note => note[options.sortBy]], ['desc']);
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return data;
        }
    },
    ranking: (data) => {
        try {
            let ranking = 1;
            for (let note of data) {
                note.rank = ranking++;
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return data;
        }
    },
    noteDataMapping: async (notes) => {
        let notesTemp = [];
        try {
            let users = await database.child('users').once('value');
            users = users.val();
            for (let note of notes) {
                note.name = note.caption;
                helpers.getOwner(users, note);
                notesTemp.push(note);
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return notesTemp;
        }
    },
    getOwner: (users, note) => {
        try {
            if (users) {
                let owner = users[note.owner];
                note.writername = owner ? owner.display_name : null;
                note.ownerDetail = owner;
            }
        } catch (exception) {
            console.log(exception);
        }
    },
}