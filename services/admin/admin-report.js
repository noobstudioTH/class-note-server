const database = require('./../../utils/FirebaseConnector').database;
const moment = require('moment');

exports = module.exports = class AdminReportService {

    constructor() {
        this.dataItemList = [{
                name: 'Total',
                series: []
            },
            {
                name: 'Paid',
                series: []
            }
        ];
    }

    async getNoteOverview(options) {
        try {
            let noteRef = database.child('notes');
            let response = await noteRef.once('value');
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            const count = helper.findNoteInTime(response, options);
            helper.buildResponseData(count.total, count.privacy, this.dataItemList, options.privacy, options.queryBy);
        } catch (exception) {
            console.log(exception);
        } finally {
            return this.dataItemList;
        }
    }
}

let helper = {
    findNoteInTime: (response, options) => {
        const count = {
            total: 0,
            privacy: 0
        }
        const today = moment();
        const startOfTime = moment().startOf(options.queryBy);
        try {
            for (let key in response) {
                let note = response[key];
                if (options.subject && options.subject !== note.subject) continue;
                if (options.grade && options.grade !== note.grade) continue;
                if (options.difficulty && options.difficulty !== note.level) continue;
                if (note.timestamp && moment(note.timestamp).isBetween(startOfTime, today)) {
                    count.total++;
                    if (note.privacy === options.privacy) count.privacy++;
                }
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return count;
        }
    },

    buildResponseData: (total, privacy, dataItemList, privacyBy, queryBy) => {
        try {
            const currentDateTime = moment().format('DD/MM/YYYY HH:mm:ss');
            dataItemList[0].series.push({
                name: `Start of ${queryBy}`,
                value: 0
            }, {
                name: currentDateTime,
                value: total
            });
            dataItemList[1].name = privacyBy;
            dataItemList[1].series.push({
                name: `Start of ${queryBy}`,
                value: 0
            }, {
                name: currentDateTime,
                value: privacy
            });
        } catch (exception) {
            console.log(exception);
        }
    }
}