const database = require('./../../utils/FirebaseConnector').database;
const _ = require('lodash');

exports = module.exports = class AdminSeriesService {
    constructor() {
        this.dataItemList = {
            total: 0,
            data: []
        }
    }

    async getList(options) {
        try {
            let seriesRef = database.child('series');
            let response = await helpers.searchByCriteria(seriesRef, options);
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            let series = helpers.transformFilter(response, options);
            await helpers.getOwner(database, series);
            series = helpers.filterByOwner(options, series);
            series = helpers.sorting(options, series);
            this.dataItemList.total = series.length;
            series = series.slice(options.start, options.end);
            this.dataItemList.data = series;
        } catch (exception) {
            console.log(exception);
        } finally {
            return this.dataItemList;
        }
    }

    async getSeriesAsMaster() {
        let series = [];
        try {
            let seriesRef = database.child('series');
            let response = await seriesRef.once('value');
            if (response && !response.exists()) return this.dataItemList;
            response = response.val();
            for (let key in response) {
                let _series = response[key];
                _series.code = key;
                _series.text = _series.name;
                series.push(_series);
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return series;
        }
    }
}

let helpers = {
    searchByCriteria: async (seriesRef, options) => {
        let response = null;
        try {
            if (options) {
                options.start = options.start || 0;
                options.end = options.end || 10;
            }
            response = await seriesRef.once('value');
        } catch (exception) {
            console.log(exception);
        } finally {
            return response;
        }
    },
    transformFilter: (response, options) => {
        let series = [];
        try {
            for (let key in response) {
                let _series = response[key];
                _series.id = key;
                series.push(_series);
            }
            if (options) {
                let search = options.search;
                if (search) {
                    if (search.key) {
                        series = _.filter(series, _series => {
                            if (_series.id) {
                                return _series.id.indexOf(search.key) > -1
                            } else {
                                return null;
                            }
                        });
                    }
                    if (search.caption) {
                        series = _.filter(series, _series => {
                            if (_series.name) {
                                return _series.name.indexOf(search.caption) > -1
                            } else {
                                return null;
                            }
                        });
                    }
                }
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return series;
        }
    },
    filterByOwner: (options, series) => {
        try {
            if (options && options.search && options.search.writerName) {
                series = _.filter(series, _series => {
                    if (_series.writername) {
                        return _series.writername.indexOf(options.search.writerName) > -1
                    } else {
                        return null;
                    }
                });
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return series;
        }
    },
    sorting: (options, series) => {
        try {
            if (options && options.sortBy && options.sortType) {
                series = _.orderBy(series, [_series => _series[options.sortBy] ? _series[options.sortBy].toLowerCase() : _series[options.sortBy]], [options.sortType]);
            }
        } catch (exception) {
            console.log(exception);
        } finally {
            return series;
        }
    },
    getOwner: async (database, series) => {
        try {
            let users = await database.child('users').once('value');
            users = users.val();
            if (users) {
                for (let serie of series) {
                    let owner = users[serie.owner];
                    serie.writername = owner ? owner.display_name : null;
                }
            }
        } catch (exception) {
            console.log(exception);
        }
    }
}