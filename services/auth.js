var auth = require('../utils/FirebaseConnector').auth;

class AuthService {

    constructor() {
        this.uid = '';
    }

    getUserToken() {
        var uid = this.getUidByRole();
        return auth.createCustomToken(uid);
    }

    getAdminToken() {
        var uid = this.getUidByRole('admin');
        return auth.createCustomToken(uid);
    }

    verifyToken(token) {
        return auth.verifyIdToken(token)
        .then(function(decodedToken) {
          return decodedToken.uid;
        }).catch(function(error) {
          console.log(error);
        });
    }

    getUidByRole(role = 'user') {
        if (role == 'user') return '6iO7eKs28USzA1yUmxpQ8a9B8MU2';
        if (role == 'user2') return '-L6JTDKl1MfcZjDXt7aU';
        return 'efSiaEO1BWhXBDBLcwI67qWPA5N2';
    }
}

module.exports = AuthService