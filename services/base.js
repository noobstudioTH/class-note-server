var database = require('../utils/FirebaseConnector').database;
var _ = require('lodash');
const Collection = require('../utils/Collection')

class BaseService {
    constructor(baseRef) {
        this.baseRef = baseRef;
        this.collection = new Collection;
        this.db = database.child(this.baseRef);
    }

    all(options) {
        if (options.startIndex == undefined) options.startIndex = 1;
        if (options.endIndex == undefined) options.endIndex = 10;
        if (options.subject == undefined) options.subject = 'math';
        if (options.order_by == undefined) options.order_by = 'rating|asc';

        return this.db.once('value').then(response => response.val())
    }

    async find(id) {
        let response = await this.db.child(id).once('value');
        return response.val();
    }

    create(data) {
        return new Promise((resolve, reject) => {
            this.db.push(data).then(response => resolve(response.key)).catch(err => reject(err));
        })
    }

    update(id, data) {
        return new Promise((resolve, reject) => {
            this.db.child(id).update(data).then(response => resolve(data)).catch(err => reject(err));
        })
    }

    remove(id) {
        return new Promise((resolve, reject) => {
            this.db.child(id).remove().then(response => resolve()).catch(err => reject(err));
        })
    }
}

module.exports = BaseService;