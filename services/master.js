const BaseService = require('./base');

class MasterService extends BaseService {
    constructor() {
        super('masters');
    }

    async grades() {
        let response = await this.db.child('grades').once('value');
        return response.val();
    }

    async levels() {
        let response = await this.db.child('levels').once('value');
        return response.val();
    }

    async subjects() {
        let response = await this.db.child('subjects').once('value');
        return response.val();
    }

    async privacy() {
        let response = await this.db.child('privacy').once('value');
        return response.val();
    }

    async noteSortable() {
        let response = await this.db.child('note_sortable').once('value');
        return response.val()
    }

    async status() {
        let response = await this.db.child('status').once('value');
        return response.val();
    }
}

module.exports = MasterService