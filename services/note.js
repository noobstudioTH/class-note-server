const fs = require("fs");
const _ = require("lodash");
const BaseService = require("./base");
const { updateNoteCount, updateRatingCount } = require("../observers/userStat");
let updateSubjectSeries = require("../observers/updateSubjectSeries");
const config = require("../config");
const moment = require("moment");

let database = require("../utils/FirebaseConnector").database;

class NoteService extends BaseService {
  constructor() {
    super("notes");
  }

  async all(options, authId = null, isMyNote = false) {
    if (options.startAt == undefined) options.startAt = 0;
    if (options.size == undefined) options.size = 10;
    if (options.subject == undefined) options.subject = "*";
    if (options.sortBy == undefined) options.sortBy = "timestamp";
    if (options.search == undefined) options.search = "";

    let dataItemList = {
      total: 0,
      data: []
    };

    let query = this.db;
    let response = {};

    if (isMyNote && options.favorite !== "true") {
      response = await query
        .orderByChild("owner")
        .equalTo(authId)
        .once("value");

      if (!response.exists()) return dataItemList;

      response = response.val();

      if (options.subject != "*") {
        response = _.omitBy(response, note => {
          return note.subject != options.subject;
        });
      }
    } else {
      if (options.subject != "*") {
        query = query.orderByChild("subject").equalTo(options.subject);
      }

      response = await query.once("value");

      if (!response.exists()) return dataItemList;

      response = response.val();
      // Only public privacy notes are shown in Class Note
      response = _.omitBy(response, note => {
        return note.privacy === 'private' || note.status !== 'approve';
      });
    }

    if (_.isEmpty(response)) return dataItemList;

    if (response.__wrapped__ != undefined) response = response.__wrapped__;

    // Transform
    let notes = [];
    for (let key in response) {
      let note = response[key];
      note.id = key;

      note.favorites =
        note.favorites != undefined
          ? note.favorites.indexOf(authId) > -1
          : false;

      // Favorite filter
      if (options.favorite === "true" && !note.favorites) continue;

      if (note.rating != undefined) {
        if (isMyNote) {
          note.rating = note.rating[authId] || 0;
        } else {
          let length = Object.keys(note.rating).length;
          let sumRating = _(note.rating)
            .values()
            .sum();
          note.rating = sumRating / length;
        }
      } else {
        note.rating = 0;
      }

      if (note.paid != undefined) {
        note.paid = note.paid.indexOf(authId) > -1;
      } else {
        note.paid = false;
      }

      if (note.media != undefined) {
        let media = [];
        for (let mediaId in note.media) {
          let tempMedia = {
            id: mediaId
          };
          tempMedia.image =
            config.resource_url.image + note.media[mediaId].image;
          if (note.media[mediaId].audio)
            tempMedia.audio =
              config.resource_url.audio + note.media[mediaId].audio;
          media.push(tempMedia);
        }
        note.media = media;
        if (!isMyNote) notes.push(note);
      }

      if (isMyNote) notes.push(note);
    }

    // Searching
    if (options.search != "") {
      await this.saveSearch(options.search);
      notes = notes.filter(note => {
        let searchKey = options.search.toLowerCase();
        if (
          note.caption.toLowerCase().indexOf(searchKey) > -1 ||
          (note.keywords &&
            note.keywords.some(
              keyword => keyword.toLowerCase().indexOf(searchKey) > -1
            ))
        ) {
          return true;
        } else return false;
      });
    }

    // Sort by
    switch (options.sortBy) {
      case "rating":
        notes = _.sortBy(notes, ["rating"]).reverse();
        break;
      case "timestamp":
        notes = notes.reverse();
        break;
      case "Title/Caption":
        notes = _.sortBy(notes, ["caption"]);
        break;
    }

    dataItemList.total = notes.length;

    // Range
    if (options.startAt != 0) {
      let start = options.startAt - 1;
      let end = start + parseInt(options.size);
      notes = notes.slice(start, end);
    }

    dataItemList.data = notes;

    return dataItemList;
  }

  async findAll(authId) {
    let response = await this.db.once("value");
    if (!response.exists()) return {};
    response = response.val();
    let notes = [];
    try {
      for (let key in response) {
        let note = response[key];
        note.id = key;
        if (note.rating != undefined) {
          note.rating = note.rating[authId] || 0;
        } else {
          note.rating = 0;
        }
        note.favorites =
          note.favorites != undefined
            ? note.favorites.indexOf(authId) > -1
            : false;
        if (note.paid != undefined) {
          note.paid = note.paid.indexOf(authId) > -1;
        } else {
          note.paid = false;
        }
        if (note.media != undefined) {
          let media = [];
          for (let mediaId in note.media) {
            let tempMedia = {
              id: mediaId
            };
            tempMedia.image =
              config.resource_url.image + note.media[mediaId].image;
            if (note.media[mediaId].audio)
              tempMedia.audio =
                config.resource_url.audio + note.media[mediaId].audio;
            media.push(tempMedia);
          }
          note.media = media;
        } else {
          note.media = [];
        }
        notes.push(note);
      }
    } catch (exception) {
      console.log(exception);
    }
    return notes;
  }

  async find(id, authId) {
    let response = await this.db.child(id).once("value");

    if (!response.exists()) return {};
    let note = response.val();

    note.id = id;

    if (note.rating != undefined) {
      note.rating = note.rating[authId] || 0;
    } else {
      note.rating = 0;
    }

    note.favorites =
      note.favorites != undefined ? note.favorites.indexOf(authId) > -1 : false;

    if (note.paid != undefined) {
      note.paid = note.paid.indexOf(authId) > -1;
    } else {
      note.paid = false;
    }

    if (note.media != undefined) {
      let media = [];
      for (let mediaId in note.media) {
        let tempMedia = {
          id: mediaId
        };
        tempMedia.image = config.resource_url.image + note.media[mediaId].image;
        if (note.media[mediaId].audio)
          tempMedia.audio =
            config.resource_url.audio + note.media[mediaId].audio;
        media.push(tempMedia);
      }
      note.media = media;
    } else {
      note.media = [];
    }

    return note;
  }

  async create(data, authId) {
    data.owner = authId;
    data.timestamp = new Date().toISOString();

    let seriesId = null;

    data.status = data.privacy == "paid" ? "approve" : "pending";

    if (data.series_id != undefined) {
      seriesId = data.series_id;
      delete data.series_id;
    }

    if (data.privacy === "paid") {
      data.status = "pending";
    } else {
      data.status = "approve";
    }

    let note = await this.db.push(data);

    if (seriesId) {
      let response = await database
        .child("series/" + seriesId + "/notes")
        .once("value");

      let notes = response.exists() ? response.val() : [];

      notes = [...new Set(notes.concat([note.key]))];

      await database.child("series/" + seriesId + "/notes").set(notes);

      await updateSubjectSeries(database.child("series/" + seriesId));
    }

    await updateNoteCount(authId, this.db);

    return note.key;
  }

  async remove(id, authId) {
    let response = await this.db.child(id).once("value");
    let note = response.val();

    let queueRemoveMedia = [];
    if (note.media != undefined) {
      try {
        for (let mediaId in note.media) {
          queueRemoveMedia.push(
            fs.unlinkSync(config.path.image + "/" + note.media[mediaId].image)
          );
          if (note.media[mediaId].audio != undefined) {
            queueRemoveMedia.push(
              fs.unlinkSync(config.path.audio + "/" + note.media[mediaId].audio)
            );
          }
        }
        await Promise.all(queueRemoveMedia);
      } catch (exception) {
        console.log(exception);
      }
    }

    await this.db.child(id).remove();

    await updateNoteCount(authId, this.db);
    await updateRatingCount(authId, this.db);

    return true;
  }

  // toggle favorite
  async favorite(noteId, authId) {
    let isFavorited;
    await this.db.child(noteId).transaction(note => {
      if (note == null) return 0;

      if (note.favorites == null) note.favorites = [];

      let index = note.favorites.findIndex(authIdHasFavorited => {
        return authIdHasFavorited == authId;
      });

      if (index >= 0) {
        note.favorites.splice(index, 1);
        isFavorited = false;
      } else {
        note.favorites.push(authId);
        isFavorited = true;
      }

      return note;
    });

    return isFavorited;
  }

  async rating(noteId, authId, rating) {
    await this.db.child(noteId + "/rating/" + authId).set(rating);
    await updateRatingCount(authId, this.db);
    return true;
  }

  async uploadImage(noteId, filename) {
    let response = await this.db.child(noteId + "/media").push({
      image: filename
    });
    return response.key;
  }

  async removeMedia(noteId, mediaId) {
    let response = await this.db
      .child(noteId + "/media/" + mediaId)
      .once("value");
    let media = response.val();
    if (media.image != undefined)
      await fs.unlinkSync(config.path.image + "/" + media.image);
    if (media.audio != undefined)
      await fs.unlinkSync(config.path.audio + "/" + media.audio);
    await this.db.child(noteId + "/media/" + mediaId).remove();
    return true;
  }

  async uploadAudio(noteId, mediaId, filename) {
    await this.db.child(noteId + "/media/" + mediaId).update({
      audio: filename
    });
    return true;
  }

  async removeAudio(noteId, mediaId) {
    try {
      let response = await this.db
        .child(noteId + "/media/" + mediaId + "/audio")
        .once("value");
      if (response.exists()) {
        audioFile = response.val();
        await fs.unlinkSync(path.audio + "/" + audioFile);
      }
      return true;
    } catch (error) {
      return false;
    }
  }

  async report(noteId, message) {
    try {
      let reportRef = database.child("report");
      let noteRef = this.db.child(noteId + "/report");
      let response = await noteRef.once("value");
      let data = {
        message: message,
        timestamp: moment().format()
      };
      let reportKey = response.val();
      if (reportKey) {
        let reportKey = response.val();
        let report = await reportRef.child(reportKey).once("value");
        await reportRef.child(reportKey).push(data);
      } else {
        let reportRes = await reportRef.push();
        await reportRef.child(reportRes.key).push(data);
        await noteRef.set(reportRes.key);
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  async saveSearch(key) {
    try {
      let searchKeyRef = database.child("searchKey");
      let response = await searchKeyRef
        .orderByChild("keyword")
        .equalTo(key)
        .once("value");
      if (response && response.exists()) {
        response = response.val();
        for (let key in response) {
          let search = response[key];
          search.frequency = search.frequency + 1;
          search.timestamp = moment().format();
          await database.child("/searchKey/" + key).update(search);
        }
      } else {
        searchKeyRef.push({
          keyword: key,
          frequency: 1,
          timestamp: moment().format()
        });
      }
    } catch (exception) {
      console.log(exception);
    }
  }
}

module.exports = NoteService;
