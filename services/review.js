const _ = require('lodash');
const BaseService = require('./base');
const UserService = require('./user');

let userService = new UserService;

class ReviewService extends BaseService {
    constructor() {
        super('reviews');
    }

    async all(noteId, options, userId) {
        if (options.startAt == undefined) options.startAt = 0;
        if (options.size == undefined) options.size = 10;
        let dataItemList = { total: 0, data: [] };
        let response = await this.db.child(noteId).once('value');
        
        if (!response.exists()) return dataItemList;

        response = response.val();

        // Transform
        let reviews = [];
        for (let key in response) {
            let review = response[key];
            review.id = key;

            // Like transform to boolean
            review.like = review.like != undefined ? (review.like.indexOf(userId) > -1) : false;

            // Reviewer transform to user object
            let reviewer = await userService.find(review.reviewer);
            review.reviewer = {display_name: reviewer.display_name} ;

            reviews.push(review);
        }

        reviews = reviews.reverse();

        dataItemList.total = reviews.length;

        // Range
        if (options.startAt != 0) {
            let start = options.startAt - 1;
            let end = start + parseInt(options.size);
            reviews = reviews.slice(start, end);
        }

        dataItemList.data = reviews;

        return dataItemList;
    }

    async post(noteId, message, userId) {
        message.reviewer = userId;
        message.timestamp = new Date().toISOString();

        await this.db.child(noteId).push(message);

        return true;
    }

    async like(noteId, reviewId, userId) {

        let isLiked;

        await this.db.child(noteId + '/' + reviewId).transaction((review) => {
            if (review == null) return 0;
            
            if (review.like == null) review.like = [];
            
            let index = review.like.findIndex((user) => { return user == userId });

            if (index >= 0) {
                review.like.splice(index, 1);
                isLiked = false;
            } else {
                review.like.push(userId);
                isLiked = true;
            }
            
            return review;
        });

        return isLiked;
    }
}

module.exports = ReviewService