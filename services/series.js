const _ = require("lodash");
const BaseService = require("./base");
let NoteService = require("./note");
let noteService = new NoteService();
let MasterService = require("./master");
let masterService = new MasterService();
let updateSubjectSeries = require("../observers/updateSubjectSeries");
let { updatSeriesCount } = require("../observers/userStat");
const moment = require("moment");
const database = require("../utils/FirebaseConnector").database;

class SeriesService extends BaseService {
  constructor() {
    super("series");
  }

  async all(options, authId = null, isMyNote = false) {
    if (options.startAt == undefined) options.startAt = 0;
    if (options.size == undefined) options.size = 10;
    if (options.subject == undefined) options.subject = "*";
    if (options.sortBy == undefined) options.order_by = "timestamp";
    if (options.search == undefined) options.search = "";

    let query = this.db;
    let response = {};
    let dataItemList = {
      total: 0,
      data: []
    };

    if (isMyNote && options.favorite !== "true") {
      response = await query
        .orderByChild("owner")
        .equalTo(authId)
        .once("value");

      if (!response.exists()) return dataItemList;

      response = response.val();

      if (options.subject != "*") {
        response = _.omitBy(response, series => {
          return series.subjects.indexOf(options.subject) > -1;
        });
      }
    } else {
      if (options.subject != "*") {
        query = query.orderByChild("subjects/" + options.subject).equalTo(true);
      }

      response = await query.once("value");

      if (!response.exists()) return dataItemList;

      response = response.val();
    }

    if (_.isEmpty(response)) return dataItemList;

    if (response.__wrapped__ != undefined) response = response.__wrapped__;

    // Transform
    let series = [];
    let tempNotes = await noteService.findAll(authId);
    let subjects = await masterService.subjects();
    let grades = await masterService.grades();
    for (let key in response) {
      let s = response[key];
      s.id = key;

      s.favorites =
        s.favorites != undefined ? s.favorites.indexOf(authId) > -1 : false;

      // Favorite filter
      if (options.favorite === "true" && !s.favorites) continue;

      if (s.rating != undefined) {
        s.selfRating = s.rating[authId];
        let length = Object.keys(s.rating).length;
        let sumRating = _(s.rating)
          .values()
          .sum();
        s.rating = sumRating / length;
      } else {
        s.rating = 0;
      }

      if (s.notes != undefined) {
        let notes = [];
        let notesDeleted = [];

        for (let i = 0; i < s.notes.length; i++) {
          let tempNote = tempNotes.find(
            _tempNote => _tempNote.id === s.notes[i]
          );
          if (_.isEmpty(tempNote)) {
            notesDeleted.push(s.notes[i]);
          } else {
            let subject = subjects.find(_ => _.code === tempNote.subject);
            let grade = grades.find(_ => _.code === tempNote.grade);
            tempNote.subject = subject ? subject.text : tempNote.subject;
            tempNote.grade = grade ? grade.text : tempNote.grade;
            notes.push(tempNote);
          }
        }

        if (notesDeleted.length > 0) await this.removeNotes(s.id, notesDeleted);

        s.notes = notes;
      } else {
        s.notes = [];
      }

      series.push(s);
    }

    // Searching
    if (options.search != "") {
      series = series.filter(serie => {
        return (
          serie.name.toLowerCase().indexOf(options.search.toLowerCase()) > -1
        );
      });
    }

    // sortBy
    switch (options.sortBy) {
      case "rating":
        series = _.sortBy(series, ["rating"]);
        break;
      case "timestamp":
        series = series.reverse();
        break;
      case "Title/Caption":
        series = _.sortBy(series, ["name"]);
        break;
    }

    dataItemList.total = series.length;

    // Range
    if (options.startAt != 0) {
      let start = options.startAt - 1;
      let end = start + parseInt(options.size);
      series = series.slice(start, end);
    }

    dataItemList.data = series;
    return dataItemList;
  }

  async find(id, authId) {
    let response = await this.db.child(id).once("value");
    let series = response.val();
    try {
      series.id = id;
      series.favorites =
        series.favorites != undefined
          ? series.favorites.indexOf(authId) > -1
          : false;
      series.rating = series.rating != undefined ? series.rating[authId] : 0;
      if (series.notes != undefined) {
        let notes = [];
        let notesDeleted = [];
        let subjects = await masterService.subjects();
        let grades = await masterService.grades();
        for (let i = 0; i < series.notes.length; i++) {
          let tempNote = await noteService.find(series.notes[i], authId);
          if (_.isEmpty(tempNote)) {
            notesDeleted.push(series.notes[i]);
          } else {
            let subject = subjects.find(_ => _.code === tempNote.subject);
            let grade = grades.find(_ => _.code === tempNote.grade);
            tempNote.subject = subject ? subject.text : tempNote.subject;
            tempNote.grade = grade ? grade.text : tempNote.grade;
            notes.push(tempNote);
          }
        }
        if (notesDeleted.length > 0)
          await this.removeNotes(series.id, notesDeleted);
        series.notes = notes;
      } else {
        series.notes = [];
      }
    } catch (exception) {
      console.log(exception);
    } finally {
      return series;
    }
  }

  async create(data, authId) {
    data.owner = authId;
    data.timestamp = new Date().toISOString();

    try {
      let subjects = {};
      data.subjects = subjects;
      for (let note of data.notes) {
        let noteResponse = await database
          .child("notes/" + note + "/subject")
          .once("value");
        noteResponse = noteResponse.val();
        subjects[noteResponse] = true;
      }
    } catch (exception) {
      console.log(exception);
    }

    let series = await this.db.push(data);
    updatSeriesCount(authId, this.db);

    return series.key;
  }

  async removeSeries(authId) {
    await updatSeriesCount(authId, this.db);
  }

  async addNotes(seriesId, newNotes) {
    let response = await this.db.child(seriesId + "/notes").once("value");

    let notes = response.exists() ? response.val() : [];

    notes = [...new Set(notes.concat(newNotes))];

    await this.db.child(seriesId + "/notes").set(notes);

    await updateSubjectSeries(this.db.child(seriesId));

    return true;
  }

  async removeNotes(seriesId, removeNotes) {
    let response = await this.db.child(seriesId + "/notes").once("value");

    let notes = response.exists() ? response.val() : [];

    notes = notes.filter(i => {
      return removeNotes.indexOf(i) < 0;
    });

    await this.db.child(seriesId + "/notes").set(notes);

    await updateSubjectSeries(this.db.child(seriesId));

    return true;
  }

  async updateSeries(series) {
    try {
      let response = await this.db.child(series.id).once("value");
      response = response.val();
      response.name = series.name;
      response.description = series.description;
      response.notes = series.notes;
      try {
        let subjects = {};
        response.subjects = subjects;
        for (let note of series.notes) {
          let noteResponse = await database
            .child("notes/" + note + "/subject")
            .once("value");
          noteResponse = noteResponse.val();
          subjects[noteResponse] = true;
        }
      } catch (exception) {
        console.log(exception);
      }
      response.timestamp = moment().format(moment.HTML5_FMT.DATETIME_LOCAL_MS);
      await this.update(series.id, response);
    } catch (exception) {
      console.log(exception);
    }
  }

  // toggle favorite
  async favorite(seriesId, authId) {
    let isFavorited;
    await this.db.child(seriesId).transaction(series => {
      if (series == null) return 0;

      if (series.favorites == null) series.favorites = [];
      let index = series.favorites.findIndex(authIdHasFavorited => {
        return authIdHasFavorited == authId;
      });

      if (index >= 0) {
        series.favorites.splice(index, 1);
        isFavorited = false;
      } else {
        series.favorites.push(authId);
        isFavorited = true;
      }

      return series;
    });
    return isFavorited;
  }

  rating(seriesId, authId, rating) {
    return this.db.child(seriesId).transaction(series => {
      if (series == null) return 0;

      if (series.rating == null) series.rating = {};

      series.rating[authId] = rating;

      return series;
    });
  }
}

module.exports = SeriesService;
