const BaseService = require('./base');
var auth = require('../utils/FirebaseConnector').auth;

const initialUserStat = {
    follower: 0,
    following: 0,
    note_count: 0,
    rating_avg: 0,
    series_count: 0
}

const initialUserCoin = {
    dcoin: 0,
    grit: 0,
    xcoin: 0
}
class UserService extends BaseService {
    constructor() {
        super('users');
    }

    auth(idToken) {
        var uid = '6iO7eKs28USzA1yUmxpQ8a9B8MU2';
        return auth.createCustomToken(uid);
    }

    async find(id, authId) {
        let response = await this.db.child(id).once('value');

        if (!response.val()) throw "Not found user";

        let user = response.val();

        if (user.stat == undefined) {
            user.stat = initialUserStat;
        }

        if (user.coin == undefined) {
            user.coin = initialUserCoin
        }

        // user.follower = user.follower != undefined ? user.follower.indexOf(authId) > -1 : false;
        const followers = user.follower;
        if (followers && followers.length > 0) {
            user.follower = followers.includes(authId);
        } else {
            user.follower = false;
        }
        return user;
    }

    async follow(userId, userAuthId) {
        let isFolllowed;
        await this.db.child(userId).transaction((user) => {
            if (user == null) return 0;

            if (user.follower == null) user.follower = [];

            let index = user.follower.findIndex((follower) => {
                return follower == userAuthId
            });

            if (index >= 0) {
                user.follower.splice(index, 1);
                isFolllowed = false;
            } else {
                user.follower.push(userAuthId);
                isFolllowed = true;
            }

            if (user.stat == null) user.stat = {};
            user.stat.follower = user.follower.length;

            return user;
        });

        await this.db.child(userAuthId).transaction((user) => {
            if (user == null) return 0;

            if (user.following == null) user.following = [];

            let index = user.following.findIndex((following) => {
                return following == userId
            });

            if (index >= 0) {
                user.following.splice(index, 1);
            } else {
                user.following.push(userId);
            }

            if (user.stat == null) user.stat = {};
            user.stat.following = user.following.length;

            return user;
        });

        return isFolllowed;
    }
}

module.exports = UserService