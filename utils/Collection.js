var _ = require('lodash');

class Collection {
    constructor() {
        this.item = [];
    }

    set(data, key) {
        if (data == null) return this;

        this.clear();

        if (data.constructor === Array) {
            this.appends(data);
        } else {
            this.add(data, key)
        }

        return this;
    }

    appends(data) {
        if (data == null) return this;

        _.forOwn(data, (value, key) => {
            this.add(value, key);
        });

        return this;
    }

    clear() {
        this.item = [];

        return this;
    }

    add(object, key) {
        this.item.push(Object.assign({id: key}, object));

        return this;
    }

    toArray() {
        return this.item;
    }

    first() {
        return this.item[0] || null;
    }
}

module.exports = Collection;