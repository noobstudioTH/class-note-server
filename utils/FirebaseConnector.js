var path = require('path');
var admin = require('firebase-admin');

require('dotenv').config()

var serviceAccount = require('../serviceAccountKey.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    // storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
});

var database = (process.env.DEV_MODE == "true") ? admin.database().ref('sandbox') : admin.database().ref();

module.exports = {
    database: database,
    auth: admin.auth()
};