let database = require('../utils/FirebaseConnector').database;

const isHasSubject = async (subjectCode) => {
    let response = await database.child('masters/subjects').orderByChild('code').equalTo(subjectCode).once('value');
    return response.exists();
}

const isHasGrade = async (gradeCode) => {
    let response = await database.child('masters/grades').orderByChild('code').equalTo(gradeCode).once('value');
    return response.exists();
}

const isHasLevel = async (levelCode) => {
    let response = await database.child('masters/levels').orderByChild('code').equalTo(levelCode).once('value');
    return response.exists();
}

const isHasPrivacy = async (privacyCode) => {
    let response = await database.child('masters/privacy').orderByChild('code').equalTo(privacyCode).once('value');
    return response.exists();
}

module.exports = { isHasSubject, isHasGrade, isHasLevel, isHasPrivacy };