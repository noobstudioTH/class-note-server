let database = require('../utils/FirebaseConnector').database;

const isHasNote = async (noteId) => {
    let response = await database.child('notes/' + noteId).once('value');
    return response.exists();
}

const isHasMedia = async (noteId, mediaId) => {
    let response = await database.child('notes/' + noteId + '/media/' + mediaId).once('value');
    return response.exists();
}

const isHasReview = async (noteId, reviewId) => {
    let response = await database.child('reviews/' + noteId + '/' + reviewId).once('value');
    return response.exists();
}

module.exports = { isHasNote, isHasMedia, isHasReview };