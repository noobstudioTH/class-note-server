let database = require('../utils/FirebaseConnector').database;

const isHasSeries = async (seriesId) => {
    let response = await database.child('series/' + seriesId).once('value');
    return response.exists();
}

module.exports = { isHasSeries };